# ap-empire_Automation

Skills Test for the QA Engineer Exam of August99 by Jon Andre V. Vergara

Automates a number of provided scenarios in the ap-empire website

Pre-requisites:
- Make sure that node.js is installed on the machine, one can get a copy of node.js here: https://nodejs.org/en/. Select the correct installer dependent on the OS.
- To verify installation of node.js, go to your terminal (powershell in Windows) and type in 'node -v' and 'npm -v'. Those should return version numbers of the installed 'node' and 'npm' applications

To install the solution:
1. Download the solution from GitLab (via Download source code button)
2. Extract the solution and take note of the directory of where it was extracted
3. Open a terminal then go to the directory of the solution
4. Run the following script: 'npm install' so that it installs all the necessary packages

To run the solution:
1. Open a termainl then go to the directory of the solution
2. Run the following script: 'npm run test'
