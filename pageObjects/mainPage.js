class mainPage{

    // This section lists all relevant selectors

    get headerLogo(){
        return $('.header-logo');
    };

    get dropDownMenu(){
        return $('#content-listings select');
    };

    get listedPricesArray(){
        return $$('.list .list-price span')
    };

    get listedPropertyTitleArray(){
        return $$('.list .prop-title');
    };

    get propertyPictureArray(){
        return $$('#content-listings .grid .listing-item img:nth-child(1)');
    };

    //==========================================
    // Interaction methods

    clickDropDownMenu(){
        this.dropDownMenu.waitForDisplayed();
        this.dropDownMenu.click();
    };

    /**
     * This method clicks the option with the matching text value
     * @param {String} text 
     */
    clickDropDownMenuItem(text){
        this.dropDownMenu.selectByVisibleText(text);
    };

    /**
     * This method gets the prices of each property and returns an array of it
     */

    getPriceArray(){
        var priceArray = [];
        for(var index = 0; index < this.listedPricesArray.length; index +=1){
            priceArray.push(parseInt(this.listedPricesArray[index].getText().substring(1).split(',').join('')));
        };
        return priceArray;
    };

    /**
     * This method gets the titles of each property and returns an array of it
     */

    getPropertyTitleArray(){
        var propertyTitleArray = [];
        for(var index = 0; index < this.listedPropertyTitleArray.length; index+=1){
            propertyTitleArray.push(this.listedPropertyTitleArray[index].getText());
        };
        return propertyTitleArray;
    };

    /**
     * This method gets the width attribute of every image
     */

    getImageWidthAttribute(){
        var imageWidthArray = [];
        for(var index = 0; index < this.propertyPictureArray.length; index+=1){
            imageWidthArray.push(parseInt(this.propertyPictureArray[index].getAttribute("width")));
        };
        return imageWidthArray;
    };

};

module.exports = new mainPage();