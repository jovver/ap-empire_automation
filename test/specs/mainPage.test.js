var main = require('../../pageObjects/mainPage');
const { assert, expect } = require('chai');

describe('User opens "https://ap-empire.agentimage.com/properties/"', ()=>{

    

    describe('When the user opens the "Sort by" dropdown', ()=>{
        
        tests = [
            {selection: "Price Descending", 
            page: "https://ap-empire.agentimage.com/properties/?sort=price&order=desc&view=List", 
            expects: "Then the properties on the page must be ordered from most to least expensive"},

            {selection: "Price Ascending", 
            page: "https://ap-empire.agentimage.com/properties/?sort=price&order=asc&view=List", 
            expects: "Then the properties on the page must be ordered from least to most expensive"},

            {selection: "A-Z", 
            page: "https://ap-empire.agentimage.com/properties/?sort=title&order=asc&view=List",
            expects: "Then the properties on the page must be shown alphabetically"},

            {selection: "Z-A", 
            page: "https://ap-empire.agentimage.com/properties/?sort=title&order=desc&view=List",
            expects: "Then the properties on the page must be shown in reverse alphabetical order"}
        ]

        tests.forEach(function(test){
            describe('And the user selects ' + test.selection, function(){
                it(test.expects, function(){
                    var initialArray = [];
                    var finalArray = [];
                    var check;

                    browser.url('');
                    browser.waitUntil(()=>{
                        return browser.getTitle() == "Properties - Jessica Royce";
                    }, "Expect the page to have loaded.");
                    check = test.page;
                    

                    switch(check.includes('price')){
                        case true:
                            main.headerLogo.waitForDisplayed();
                            main.listedPricesArray;
                            initialArray = main.getPriceArray();
                            browser.navigateTo(test.page);
                            main.headerLogo.waitForDisplayed();
                            finalArray = main.getPriceArray();

                            if(check.includes('desc')){
                                initialArray.sort(function(a,b){return b-a});
                            }
                            else{
                                initialArray = initialArray.sort(function(a,b){return a-b});
                            };
                            break;

                        default:
                            main.headerLogo.waitForDisplayed();
                            main.listedPricesArray;
                            initialArray = main.getPropertyTitleArray();
                            browser.navigateTo(test.page);
                            main.headerLogo.waitForDisplayed();
                            finalArray = main.getPropertyTitleArray();

                            if(check.includes('desc')){
                                initialArray.sort();
                                initialArray
                            }
                            else{
                                initialArray.reverse();
                            };

                    };
                    expect(initialArray).to.be.ordered.members(finalArray, "List is not sorted correctly");
                });

            });
        });
        
    });

    describe('Given the page finishes loading', ()=>{
        it('Then all images must be 300px wide', ()=>{
            browser.url('');
            browser.waitUntil(()=>{
                return browser.getTitle() == "Properties - Jessica Royce";
            }, "Expect the page to have loaded.");
            
            expect(main.getImageWidthAttribute()).to.be.an('array').that.include(300);
        });
    });

});